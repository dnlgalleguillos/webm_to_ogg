# WEBM to OGG

![webm_to_ogg](/uploads/f306bd03da4554e42da7e00b6b22fe2f/webm_to_ogg.png)

## convert .webm files to .OGG on Linux with GNOME Desktop. 
-   [Debian](https://www.debian.org/)
-   [Fedora](https://fedoraproject.org/)

Here's a simple application that converts .webm files to .OGG using ffmpeg in Python.

## OGG
[OGG](https://xiph.org/) is a free, open container format maintained by the Xiph.Org Foundation. The authors of the Ogg format state that it is unrestricted by software patents and is designed to provide for efficient streaming and manipulation of high-quality digital multimedia. Its name is derived from "ogging", jargon from the computer game Netrek.

## Requirements
- [Python](https://www.python.org/) installed on your Linux system.
- [ffmpeg](https://ffmpeg.org/) installed on your system.
- [tkinter](https://docs.python.org/3/library/tkinter.html) installed on your system.

## Steps to Install ffmpeg for Debian
```
sudo apt-get update
```
```
sudo apt-get install ffmpeg
```
## Steps to Install ffmpeg for Fedora
```
sudo dnf update
```
```
sudo dnf install ffmpeg.x86_64
```
## Steps to Install tkinter for Debian
```
sudo apt-get install python3-tk
```
## Steps to Install tkinter for Fedora
```
sudo dnf install python3-tkinter.x86_64
```
## Python Script:
```
import os
import subprocess
import tkinter as tk
from tkinter import filedialog, messagebox

def convert_webm_to_ogg(input_file, output_file):
    try:
        # Run the ffmpeg command to convert webm to ogg
        subprocess.run(['ffmpeg', '-i', input_file, output_file], check=True)
        messagebox.showinfo("Success", f"Conversion successful!\n{output_file}")
    except subprocess.CalledProcessError as e:
        messagebox.showerror("Error", f"Conversion failed: {e}")

def select_input_file():
    input_file = filedialog.askopenfilename(filetypes=[("webm files", "*.webm")])
    if input_file:
        entry_input.delete(0, tk.END)
        entry_input.insert(0, input_file)

def select_output_file():
    output_file = filedialog.asksaveasfilename(defaultextension=".ogg", filetypes=[("ogg files", "*.ogg")])
    if output_file:
        entry_output.delete(0, tk.END)
        entry_output.insert(0, output_file)

def start_conversion():
    input_file = entry_input.get()
    output_file = entry_output.get()
    if input_file and output_file:
        convert_webm_to_ogg(input_file, output_file)
    else:
        messagebox.showwarning("Warning", "Please select both input and output files.")

# Create the main window
root = tk.Tk()
root.title("webm to ogg Converter")

# Create and place the input file selection widgets
label_input = tk.Label(root, text="Input .webm file:")
label_input.grid(row=0, column=0, padx=10, pady=5, sticky="e")

entry_input = tk.Entry(root, width=50)
entry_input.grid(row=0, column=1, padx=10, pady=5)

button_input = tk.Button(root, text="Browse...", command=select_input_file)
button_input.grid(row=0, column=2, padx=10, pady=5)

# Create and place the output file selection widgets
label_output = tk.Label(root, text="Output .ogg file:")
label_output.grid(row=1, column=0, padx=10, pady=5, sticky="e")

entry_output = tk.Entry(root, width=50)
entry_output.grid(row=1, column=1, padx=10, pady=5)

button_output = tk.Button(root, text="Browse...", command=select_output_file)
button_output.grid(row=1, column=2, padx=10, pady=5)

# Create and place the convert button
button_convert = tk.Button(root, text="Convert", command=start_conversion)
button_convert.grid(row=2, column=0, columnspan=3, pady=10)

# Start the Tkinter event loop
root.mainloop()

```
## Explanation:
1. Install ffmpeg: Use the command provided to install ffmpeg.
2. Script:
    - Prompts the user for input and output file paths.
    - Uses subprocess.run to call ffmpeg for the conversion.
    - Handles errors and confirms successful conversion.
3. tkinter library, which is usually included with Python installations. If it's not installed, you can install it via your package manager.

## Running the Application:
1. Save the script as webm_to_ogg.py.
2. Run the script:
```
python3 webm_to_ogg.py
```
3. Enter the required file paths when prompted.
```
Input webm file:
```
```
Output ogg file:
```
This script provides a simple interface for converting .webm files to .ogg using ffmpeg.

## The Powerful of **ffmpeg**

To convert a .webm file to an .ogg file using ffmpeg on a Linux system, you can use the following command:
```
ffmpeg -i input.webm -vn -ab 128k output.ogg
```
While you can use the ffmpeg command line, this script offers a faster and more automated solution.