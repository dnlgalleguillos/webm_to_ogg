import os
import subprocess
import tkinter as tk
from tkinter import filedialog, messagebox

def convert_webm_to_ogg(input_file, output_file):
    try:
        # Run the ffmpeg command to convert webm to ogg
        subprocess.run(['ffmpeg', '-i', input_file, output_file], check=True)
        messagebox.showinfo("Success", f"Conversion successful!\n{output_file}")
    except subprocess.CalledProcessError as e:
        messagebox.showerror("Error", f"Conversion failed: {e}")

def select_input_file():
    input_file = filedialog.askopenfilename(filetypes=[("webm files", "*.webm")])
    if input_file:
        entry_input.delete(0, tk.END)
        entry_input.insert(0, input_file)

def select_output_file():
    output_file = filedialog.asksaveasfilename(defaultextension=".ogg", filetypes=[("ogg files", "*.ogg")])
    if output_file:
        entry_output.delete(0, tk.END)
        entry_output.insert(0, output_file)

def start_conversion():
    input_file = entry_input.get()
    output_file = entry_output.get()
    if input_file and output_file:
        convert_webm_to_ogg(input_file, output_file)
    else:
        messagebox.showwarning("Warning", "Please select both input and output files.")

# Create the main window
root = tk.Tk()
root.title("webm to ogg Converter")

# Create and place the input file selection widgets
label_input = tk.Label(root, text="Input .webm file:")
label_input.grid(row=0, column=0, padx=10, pady=5, sticky="e")

entry_input = tk.Entry(root, width=50)
entry_input.grid(row=0, column=1, padx=10, pady=5)

button_input = tk.Button(root, text="Browse...", command=select_input_file)
button_input.grid(row=0, column=2, padx=10, pady=5)

# Create and place the output file selection widgets
label_output = tk.Label(root, text="Output .ogg file:")
label_output.grid(row=1, column=0, padx=10, pady=5, sticky="e")

entry_output = tk.Entry(root, width=50)
entry_output.grid(row=1, column=1, padx=10, pady=5)

button_output = tk.Button(root, text="Browse...", command=select_output_file)
button_output.grid(row=1, column=2, padx=10, pady=5)

# Create and place the convert button
button_convert = tk.Button(root, text="Convert", command=start_conversion)
button_convert.grid(row=2, column=0, columnspan=3, pady=10)

# Start the Tkinter event loop
root.mainloop()